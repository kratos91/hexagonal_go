package service

import (
	"bitbucket.org/kratos91/hexagonal_go/domain"
	"bitbucket.org/kratos91/hexagonal_go/errs"
	"bitbucket.org/kratos91/hexagonal_go/repository"
)

type CustomerService interface {
	GetAllCustomers(string) ([]domain.Customer, *errs.AppError)
	GetCustomer(int) (*domain.Customer, *errs.AppError)
	SaveCustomer(*domain.Customer) *errs.AppError
}

type DefaultCustomerService struct {
	repo repository.CustomerRepoDb
}

func (s *DefaultCustomerService) GetAllCustomers(status string) ([]domain.Customer, *errs.AppError) {
	var i bool
	if status == "active" {
		i = true
	} else if status == "inactive" {
		i = false
	} else {
		i = true
	}

	return s.repo.FindAll(i)
}

func (s *DefaultCustomerService) GetCustomer(id int) (*domain.Customer, *errs.AppError) {
	return s.repo.ById(id)
}

func (s *DefaultCustomerService) SaveCustomer(customer *domain.Customer) *errs.AppError {
	return s.repo.Save(*customer)
}

func NewCustomerService(repository repository.CustomerRepoDb) CustomerService {
	return &DefaultCustomerService{repository}
}
