package app

import (
	"bitbucket.org/kratos91/hexagonal_go/app/client"
	"bitbucket.org/kratos91/hexagonal_go/repository"
	"bitbucket.org/kratos91/hexagonal_go/service"
	"github.com/gin-gonic/gin"
)

func Start() {
	client, err := client.Connect()
	if err != nil {
		panic(err)
	}

	ch := NewCustomerHandler(service.NewCustomerService(repository.NewCustomerRepositoryDb(client)))

	//GIN
	router := gin.Default()
	router.GET("/customers", ch.GetAllCustomers)
	router.GET("/customers/:customer_id", ch.GetCustomer)
	router.POST("/customers", ch.SaveCustomer)

	//router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	router.Run()
}
