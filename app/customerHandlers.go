package app

import (
	"net/http"
	"strconv"

	"bitbucket.org/kratos91/hexagonal_go/domain"
	"bitbucket.org/kratos91/hexagonal_go/service"
	"github.com/gin-gonic/gin"
)

type CustomerHandler interface {
	GetAllCustomers(*gin.Context)
	GetCustomer(*gin.Context)
	SaveCustomer(*gin.Context)
}

type CustomerHandlers struct {
	service service.CustomerService
}

// ObtenerClientes godoc
// @Summary Obtiene todo los clientes
// @Description Deveuelve todos los clientes de la BD
// @Tags Customers
// @Produce  json
// @Param status query string false "active/inactive/''"
// @Success 200 {object} domain.Customers
// @Failure 404 {object} errs.AppError
// @Failure 500 {object} errs.AppError
// @Router /customers [get]
func (ch *CustomerHandlers) GetAllCustomers(ctx *gin.Context) {
	status := ctx.Query("status")
	customers, err := ch.service.GetAllCustomers(status)
	if err != nil {
		ctx.JSON(err.Code, err.AsMessage())
	} else {
		ctx.JSON(http.StatusOK, customers)
	}
}

// ObtenerClientes godoc
// @Summary Obtiene un cliente
// @Description Devuelve un cliente dependiendo el id
// @Tags Customers
// @Produce  json
// @Param customer_id path string true "Id del cliente"
// @Success 200 {object} domain.Customer
// @Failure 404 {object} errs.AppError
// @Failure 500 {object} errs.AppError
// @Router /customers/{customer_id} [get]
func (ch *CustomerHandlers) GetCustomer(ctx *gin.Context) {
	id := ctx.Param("customer_id")
	i, _ := strconv.Atoi(id)
	customer, err := ch.service.GetCustomer(i)

	if err != nil {
		ctx.JSON(err.Code, err.AsMessage())
	} else {
		ctx.JSON(http.StatusOK, customer)
	}
}

func (ch *CustomerHandlers) SaveCustomer(ctx *gin.Context) {
	var customer domain.Customer
	err := ctx.BindJSON(&customer)

	if err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, err)
	}
	ctx.JSON(http.StatusOK, nil)
}

func NewCustomerHandler(service service.CustomerService) CustomerHandler {
	return &CustomerHandlers{service: service}
}
