package client

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Connect() (client *gorm.DB, err error) {
	dbs := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=true", "emiliano", "God080591.", "127.0.0.1:3306", "bank")
	client, err = gorm.Open(mysql.Open(dbs), &gorm.Config{})

	if err != nil {
		return nil, err
	}

	return client, nil
}
