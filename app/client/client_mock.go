package client

import (
	"bitbucket.org/kratos91/hexagonal_go/domain"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func ConnectMock() *gorm.DB {
	dbClient, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	dbClient.AutoMigrate(&domain.Customer{})

	return dbClient
}
