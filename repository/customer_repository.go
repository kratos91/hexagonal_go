package repository

import (
	"fmt"

	"bitbucket.org/kratos91/hexagonal_go/domain"
	"bitbucket.org/kratos91/hexagonal_go/errs"
	"bitbucket.org/kratos91/hexagonal_go/logger"
	"gorm.io/gorm"
)

type CustomerRepoDb interface {
	ById(int) (*domain.Customer, *errs.AppError)
	FindAll(bool) ([]domain.Customer, *errs.AppError)
	Save(domain.Customer) *errs.AppError
}

type CustomerRepositoryDb struct {
	client *gorm.DB
}

func (d *CustomerRepositoryDb) ById(id int) (*domain.Customer, *errs.AppError) {
	var err error
	var c domain.Customer

	err = d.client.Debug().Model(domain.Customer{}).Where("id=?", id).Take(&c).Error

	if err != nil {
		return nil, errs.NewNotFoundError(fmt.Sprintf("Customer not found: %s", err.Error()))
	}
	return &c, nil
}

func (d *CustomerRepositoryDb) FindAll(status bool) ([]domain.Customer, *errs.AppError) {

	customers := []domain.Customer{}
	rows, err := d.client.Debug().Model(&domain.Customer{}).Where("status=?", status).Rows()

	if err != nil {
		logger.Error("Error while querying customer table: " + err.Error())
		return nil, errs.NewUnxpectedError(err.Error())
	}

	for rows.Next() {
		var c domain.Customer
		rows.Scan(&c.Id, &c.Name, &c.Status)
		customers = append(customers, c)
	}

	if len(customers) == 0 {
		return nil, errs.NewNotFoundError("no records found")
	}
	return customers, nil
}

func (d *CustomerRepositoryDb) Save(customer domain.Customer) *errs.AppError {
	err := d.client.Debug().Model(&domain.Customer{}).Create(&customer).Error
	if err != nil {
		return errs.NewUnxpectedError(err.Error())
	}
	return nil
}

func NewCustomerRepositoryDb(client *gorm.DB) CustomerRepoDb {
	return &CustomerRepositoryDb{client: client}
}
