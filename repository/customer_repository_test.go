package repository

import (
	"net/http"
	"testing"

	"bitbucket.org/kratos91/hexagonal_go/app/client"
	"bitbucket.org/kratos91/hexagonal_go/domain"
	"github.com/stretchr/testify/assert"
)

func TestFindById_ErrorNotFound(t *testing.T) {
	mockClient := client.ConnectMock()
	customerRepo := NewCustomerRepositoryDb(mockClient)

	_, err := customerRepo.ById(1)

	assert.NotNil(t, err)
	assert.EqualValues(t, 404, err.Code)
	assert.EqualValues(t, "Customer not found: record not found", err.AsMessage().Message)
}

func TestFindById_NoError(t *testing.T) {
	mockClient := client.ConnectMock()
	customerRepo := NewCustomerRepositoryDb(mockClient)
	mockClient.Create(&domain.Customer{1, "Emiliano", false})

	res, err := customerRepo.ById(1)

	assert.Nil(t, err)
	assert.EqualValues(t, 1, res.Id)
	assert.EqualValues(t, "Emiliano", res.Name)
}

func TestFindAll_ErrorNotFoundRecords(t *testing.T) {
	mockClient := client.ConnectMock()
	customerRepo := NewCustomerRepositoryDb(mockClient)

	res, err := customerRepo.FindAll(true)

	assert.NotNil(t, err)
	assert.EqualValues(t, 0, len(res))
	assert.EqualValues(t, http.StatusNotFound, err.Code)
	assert.EqualValues(t, "no records found", err.AsMessage().Message)
}

func TestFindAll_NoError(t *testing.T) {
	mockClient := client.ConnectMock()
	customerRepo := NewCustomerRepositoryDb(mockClient)
	mockClient.Create(&domain.Customer{1, "Emiliano", false})
	mockClient.Create(&domain.Customer{2, "Belen", false})

	res, err := customerRepo.FindAll(false)

	assert.Nil(t, err)
	assert.EqualValues(t, 2, len(res))
}
