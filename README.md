# API con arquitectura hexagonal en Go

Ejemplo de implementación de una API REST con arquitectura hexagonal usando de router
gin gionic y swagger para documentar

Con ejemplos de test unitarios a los:
    * Handlers
    * Services
    * Repositories

## Extensiones
### Extension para el logger
* [Uber-logger] go get -u go.uber.org/zap
### Extension para simplificar el marshall de mysql
* [Gorm] go get "gorm.io/gorm"
### Extensiones para la documentación con swagger
* [Console] go get -u github.com/swaggo/swag/cmd/swag
* [Index-swagger] go get -u github.com/swaggo/http-swagger
* [Template] go get -u github.com/alecthomas/template
* [Gin-Swagger] go get -u github.com/swaggo/gin-swagger
* [Swaggo-files] go get -u github.com/swaggo/files


#### Para inicializar la documentación con swaggo
> swag init -g main.go "o" $HOME/go/bin/swag init -g main.go
