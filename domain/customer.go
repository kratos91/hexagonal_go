package domain

type Customers struct {
	Customers []Customer `json:"customers"`
}

type Customer struct {
	Id     int32  `json:"id" db:"id"`
	Name   string `json:"full_name" db:"name"`
	Status bool   `json:"status" db:"status"`
}
