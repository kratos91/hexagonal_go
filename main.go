package main

import (
	"bitbucket.org/kratos91/hexagonal_go/app"
	_ "bitbucket.org/kratos91/hexagonal_go/docs"
	"bitbucket.org/kratos91/hexagonal_go/logger"
)

// @title API Arquitectura hexagonal
// @version 1.0
// @description API banco
// @contact.name Emiliano Rubén Martínez Guzmán
// @contact.email emilianoruben91@gmail.com
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:8080
// @BasePath /
func main() {
	logger.Info("Starting de application...")
	app.Start()
}
